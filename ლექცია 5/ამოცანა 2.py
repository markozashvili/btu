# დაგიქირავათ კომპანიამ კონკრეტული მოდულის დასაწერათ, რომელთაც აქვთ გარკვეული პროექტი Python ზე. თქვენი დავალებაა დაწეროთ ვალიდაციის ფუნქცია რომელიც შეამოწმებს შემოტანილი password ის სტრინგი მისაღებია თუ.
# ვალიდაციის წესები რომელთაც password ი უნდა ესადაგებოდეს:
# * უნდა შედგებოდეს მინიმუმ 6 სიმბოლოსაგან
# * აუცილებლად უნდა ერიოს თუნდაც ერთი დიდი Letter
# * ასევე სტრინგში უნდა იყოს მინიმუმ 2 ციფრი
# * ["~!@#$%^&*()_-+=}{|[]\?/:;'<>,."] ასევე აკრძალულია ამ სიმბოლოების გამოყენება
#
# p.s თუ მომხმარებელი შემოიტანს შეცდომით password ს მიახვერდრეთ რა ეშლება.
# კოდი გააფორმეთ ფუნქციებით ანუ გქონდეთ თითეული ოპერაციისთვის შექმენილი ზოგადი ფუქნციები რომელსაც გააერთიანებთ main() ფუნქციაში.
#
# წარმატებები

def checkLength(password):
    if len(password) >= 6:
        return True
    else:
        print("Password is less than 6 characters")


def checkUpper(password):
    for letter in password:
        if letter.isupper():
            return True

    print("Password must cointain at least one uppercase character")


def checkDigits(password):
    countNum = 0
    for letter in password:
        if letter.isdigit():
            countNum += 1

    if countNum >= 2:
        return True
    else:
        print("Password must contain at least 2 digits")


def checkSpecialChar(password):
    specialChars = "\"~!@#$%^&*()_-+=}{|[]\?/:;'<>,.\""

    for letter in specialChars:
        if letter in password:
            print("Password mustn't contain special character")
            return False


    return True


def main():
    password = input("Enter Password")

    if checkSpecialChar(password) and checkDigits(password) and checkLength(password) and checkUpper(password):
        print("Password is valid")


main()
