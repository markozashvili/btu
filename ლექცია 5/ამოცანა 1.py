# პირობა:
# დაგიქირავათ რესტორანმა რომელთაც სურთ აპლიკაცია შემდეგი ფუნქციებით:
# * მომხამარებელს შესთავაზოს მენიუ
# * აარჩევინოს კერძები (რაოდენობით)
# * საბოლოოდ კი აჩვენოს შეკვეთა, და შესაბამისი ჩეკი (ანუ თანხა რაც საჭიროა შეკვეთისთვის)
#
# კოდი გააფორმეთ ფუნქციებით ანუ გქონდეთ თითეული ოპერაციისთვის შექმენილი ზოგადი ფუქნციები რომელსაც გააერთიანებთ main() ფუნქციაში.

def showMenu(menu):
    i = 1
    for key, value in menu.items():
        print(i, key, value)
        i += 1


def chooseItems():
    items = input("Which items do you want").split(",")

    return items


def chooseQuantity():
    quantity = input("How many items do you want").split(",")

    return quantity


def cheque(prices, items, quantity):
    total = 0
    for item in items:
        total = total + prices[int(item) - 1] * float(quantity[items.index(item)])
    return total


def main():
    menu = {
        "tea": 1.50,
        "coffee": 2.50,
        "juice": 3.50
    }
    prices = []

    for key, value in menu.items():
        prices.append(value)

    showMenu(menu)
    items = chooseItems()
    quantity = chooseQuantity()
    print(cheque(prices, items, quantity))


main()
