# მოცემული გაქვთ ინფორმაცია შემდეგი სახით :
#
# unknown_list = ["toby", "James", "kate", "George", "James", "rick", "Alex", "Jein", "Alex", "Alex","George", "Jein", "kate", "medelin"]
#
# თქვენი დავალებაა დაწეროთ კოდი რომელიც დაითვლის დუბლიკატების (ანუ მსგავსი ელემენტების რაოდენობას) რის შემდეგაც წაშლის კლონირებულებს და დატოვებს მხოლოდ ერთ ცალ ელემენტს კლონებიდან. ასევე იმ მომხმარებლებს რომელთაც დაავიწყდათ თავისი სახელის დაწერა დიდი ასოთი, უნდა გაუზარდოთ სახელის პირველი ასო და ისე შეინახოთ საბოლოოდ დაფორმატირებულ სიაში.

unknown_list = ["toby", "James", "kate", "George", "James", "rick", "Alex", "Jein", "Alex", "Alex","George", "Jein", "kate", "medelin"]
sia = []

for element in unknown_list:
    sia.append(element.capitalize())

for word in sia:
    count = sia.count(word)
    while count != 1:
        sia.remove(word)
        break


print(sia)