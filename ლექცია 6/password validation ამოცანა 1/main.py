from digits import *
from letters import *
from length import *
from upper import *
import os



def is_valid(password,forbidden_letters):
    if check_upper(password) and check_length(password) and check_digits(password) and not check_special_chars(password, forbidden_letters):
        return True
    else:
        return False


def main():
    forbidden_letters = "[\"~!@#$%^&*()_-+=}{|[]\?/:;'<>,\"]"
    password = input("Enter Password")
    dictionary = {}

    while not is_valid(password,forbidden_letters):

        if not check_digits(password):
            print("Password must contain 2 digits")

        if check_special_chars(password, forbidden_letters):
            print("Password mustn't contain special characters")

        if not check_length(password):
            print("Password must contain 6 characters")

        if not check_upper(password):
            print("password must contain 2 uppercase characters")

        password = input("Enter Password Again")

    print("Password is valid")

    dictionary[os.urandom(2)] = password
    print(dictionary)

if __name__ == "__main__":
    main()
