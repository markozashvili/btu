def check_digits(password):
    count_digits = 0
    for letter in password:
        if letter.isdigit():
            count_digits += 1

    if count_digits >= 2:
        return True
    else:
        return False
