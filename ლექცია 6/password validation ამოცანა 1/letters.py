def check_special_chars(password, forbiden_letters):
    for letter in forbiden_letters:
        if letter in password:
            return True

    return False
