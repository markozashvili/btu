# წინა ამოცანის შებრუნებული reversed ვერსია.
# ანუ:
# განსაზღვრეთ ნებისმიერი რიცხვი. დაწერეთ while
# ციკლი რომელიც გაეშვება ხუთჯერ. ციკლში უნდა ეცადოს კომპიუტერი გამოიცნოს თქვენი ციფრი რომელიც განსაზღვრულია ციკლის გარეთ. თუკი მესამე და სამზე მეტი
# ცდაა - ციკლშივე დაეხმარეთ კომპიუტერს რომ მისცეთ მინიშნება - მის მიერ შეყვანილი
# ციფრი ახლოა თუ არა ჩაფიქრებულ ციფრთან. თუკი გამოიცნო
# პასუხი კომპიუტერმა - გამორთე ციკლი და მიულოცეთ მას. <3

import random


def main():
    number = int(input("Enter Number 1 to 25"))
    i = 0
    hint_number = 0
    while i < 5:
        random_number = 0
        if i >= 3:
            random_number = random.randrange(hint_number - 5, hint_number + 5)
        else:
            random_number = random.randrange(1, 25)
        if number == random_number:
            print("Congratulations")
            break
        elif i >= 3 and abs(random_number - number) <= 5:
            hint_number = random_number

        print("Try Again")

        i += 1


main()
