# შემთხვევითობის პრინციპით განსაზღვრეთ რიცხვი 1 დან 25 მდე. დაწერეთ while
# ციკლი რომელიც გაეშვება ხუთჯერ. ციკლში ეცადეთ გამოიცნოთ ციკლის გარეთ
# დაგენერირებული შემთხვევითი ციფრის მნიშვნელობა. თუკი მესამე და სამზე მეტი
# ცდაა - ციკლშივე დაეხმარეთ იუზერს რომ მისცეთ მინიშნება - მის მიერ შეყვანილი
# ციფრი ახლოა თუ არა ჩაფიქრებულ ციფრთან. თუკი გამოიცნო
# პასუხი იუზერმა - გამორთე ციკლი და მიულოცეთ მას.

import random


def main():
    random_number = random.randrange(1, 5)
    num = int(input("Enter Number"))
    i = 0

    while i < 5:
        if num == random_number:
            print("Congratulations")
            break
        elif i >= 3 and abs(num - random_number) <= 5:
            print("You are close")
        else:
            print("Try Again")

        num = int(input("Enter Number"))
        i += 1


main()
