def print_menu(menu):
    dictionary = {}
    i = 0
    for item, price in menu.items():
        dictionary[str(i + 1) + " " + item] = str(price) + "$"
        i += 1
    return dictionary


def get_order(menu, order_items, order_quantities):
    order = {}
    for item, price in menu.items():
        if item in order_items:
            order[item] = order_quantities[order_items.index(item)]

    return order


def total_bill(order, menu):
    bill = 0
    for item, quantity in order.items():
        bill += menu[item] * quantity

    return bill
