# პირობა:
# დაგიქირავათ რესტორანმა რომელთაც სურთ აპლიკაცია შემდეგი ფუნქციებით:
# * მომხამარებელს შესთავაზოს მენიუ
# * აარჩევინოს კერძები (რაოდენობით)
# * საბოლოოდ კი აჩვენოს შეკვეთა, და შესაბამისი ჩეკი (ანუ თანხა რაც საჭიროა შეკვეთისთვის)
#
# შეეცადეთ ფუნქიონალური პროგრამირების სტილი დაიცვათ.
# წერისას გაითვალისწინეთ ლექციაში წარმოდგენილი მაგალითი გვერდი (5, 6)

from orders import *


def main():
    menu = {"tea": 1.50, "coffee": 2.50, "juice": 3}
    order_items = []
    order_quantities = []
    items = []
    print(print_menu(menu))

    for item, price in menu.items():
        items.append(item)

    word = input("Enter item and quantity")

    while word != "stop":
        splitted_word = word.split(",")
        order_items.append(items[int(splitted_word[0]) - 1])
        order_quantities.append(int(splitted_word[1]))
        word = input("Enter item and quantity")

    order = get_order(menu, order_items, order_quantities)
    print(order)
    print(total_bill(order, menu))


main()
