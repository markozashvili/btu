# შექმენით ფუნქცია რომელიც დააამუშავებს მნიშვნელობას და დაადგენს არის თუ არა პანგრამი .
# p.s პანგრამი ეწოდება წინადადებას რომელშიც
# გამოყენებულია ანბანის ყოველი ასო მაგ ინგლისურში ყველაზე ფართოდ გავრცელებული მაგალითია:
# The quick brown fox jumps over the lazy dog.
#
# შეზღუდვა: თუ მომხმარებელი შემოიტანს ანბანის მნიშვნელობების გარდა სხვა ნებისმიერ მნიშვნელობას მიეცით შანსი თავისი შეცდომა გამოასწოროს.
# შეეცადეთ თქვენით აღწეროთ ნებისმიერი ჩაშენებული ფუნქცია რომლის გამოყენებასაც დააპირებთ ფუნქცია.

def isPanagram(word):
    letters = "abcdefghijklmnopqrstuvwxyz"
    count = []
    for letter in letters:
        if letter not in word:
            return False

    return True


def isSpecialChar(word):
    specialChars = "!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"
    for char in specialChars:
        if char in word:
            return True

    return False


def main():
    word = input("Enter Word")
    while isSpecialChar(word):
        print("Special characters are prohibited")
        word = input("Enter Word Again")

    print(isPanagram(word))


main()
