# შემოატანინეთ მომხმარებელს 50 მათემატიკური მნიშვნელობა და შეინახეთ სიაში. რის შემდეგაც შექმნით ფუნქციას checkList(any_list) რომელიც ამოიღებს სიიდან იმ მნიშვნელობებს რომელიც იქნება მეტი მეზობელ მათემატიკურ მნიშვნელობებზე.
#
# advance:
# checkList(any_list, filter_by) ფუნქცია გახადეთ ელასტიური და გადაეცით დამატებითი პარამეტრი რომლის მიხედვით ფუნქცია გაფილტრაცს მნიშვნელობებს ანუ თუ filter_by მნიშვნელობა იქნება "bigger" ამოიღებს იმ მნიშვნელობას რომელიც მეტია მეზობლებზე ხოლო თუ filter_by - ს მნიშვნელობა იქნება "lower" ამოიღებს ისეთ მნიშვნელობებს რომელიც იქნება ორივე მეზობელზე ნაკლები.

def checkList(nums, filter_by):
    sia = []
    if filter_by.lower() == "bigger":
        for i in range(len(nums)):
            if 0 < i < len(nums) - 1 and int(nums[i]) > int(nums[i + 1]) and int(nums[i]) > int(nums[i - 1]):
                sia.append(nums[i])

    if filter_by.lower() == "lower":
        for i in range(len(nums)):
            if 0 < i < len(nums) - 1 and int(nums[i]) < int(nums[i + 1]) and int(nums[i]) < int(nums[i - 1]):
                sia.append(nums[i])

    return sia


def main():
    nums = input("Enter Nums").split(",")
    print(checkList(nums, "bigger"))


main()
