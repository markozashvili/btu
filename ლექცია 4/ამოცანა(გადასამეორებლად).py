# შექმენით ცვლადი რომელსაც კონსოლიდან მიანიჭებთ 5 ცალ
# მნიშნველობას. თითეული მნიშნველობა უნდა შეინახოთ list ში
# (list ცვლადი არ არის), რის შემდეგაც უნდა შეამოწმოთ თითეულ
# ცვლადში რამდენი ხმოვანია და ჩაწეროთ dictionary ში
# ინდექსის მიხედვით (dictionary არ არის ცვლადი).
# example of workable code:
#
# variable = 'BTU'
#
# db_list = ['BTU']
#
# db_dict = {0, '1'}

def countVowel(word):
    letters = ["a","e","i","o","u"]
    count = 0
    for letter in letters:
        if letter in word.lower():
            count += word.lower().count(letter)
            
    return count

db_list = []
db_dict = {}

while len(db_list) != 5:
    word = input("Enter Word")
    db_list.append(word)

for item in db_list:
    db_dict[db_list.index(item)] = countVowel(item)

print(db_list)
print(db_dict)
