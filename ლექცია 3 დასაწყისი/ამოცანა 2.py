# დაწერეთ კოდი რომელიც დაადგენს მოსწავლეს ეთვლება საგანი ჩაბარებულად თუ არა.
# პირობა არის შემდეგი:
# გვაქვს მოცემული Dictionary - ს ცვლადი:
#
# Journal = {"vasadze" : 78 , "shonia": 86 ,"mosiava": 45, "kalandadze":36 , "sirbiladze":64 }
#
# უნდა ამოვიღოთ თითოეული key თავისი value თი და შევამოწმოთ რომელ მოსწავლეს აქვს 51 ქულაზე მეტი საგანში (51 ქულა ითვლება ჩაბარებულად) შემდეგ კი დავახარისხოთ მოსწავლეები passed_ones = { }, not_passed_ones = { }, Dictionar ებში და დავბეჭდოთ შედეგი.
#
# hint:
# for the_key, the_value in Journal.items():
# print (the_key, 'corresponds to', the_value)


Journal = {"vasadze" : 78 , "shonia": 86 ,"mosiava": 45, "kalandadze":36 , "sirbiladze":64 }
passed_ones = {}
not_passed_ones = {}


for name,score in Journal.items():
    if(score >= 51):
        passed_ones[name] = score
    else:
        not_passed_ones[name] = score

print(passed_ones)
print(not_passed_ones)
