# გვაქვს მოცემული სია იგივე მასივი
# sia = [ "last", 2, "hey", 213, "45","start"]
# თქვენ უნდა დაწეროთ კოდი for ისა და len ის გამოყენებით რომელიც შეუცვლის მოცემულ სიას ადგილებს და დაპრინტავს შებრუნებულად.
# sia = [,"start", "45", 213, "hey", 2, "last"]
# შეგიძლიათ გამოყენება ახალი ცვლადის სუფთა მასივისთვის.
#
# შეზღუდვა :
# მხოლოდ ნასწავლი მასალით.


sia = ["last", 2, "hey", 213, "45","start"]
siaLength = len(sia)
newSia = []

for element in sia:
    elementIndex = sia.index(element)
    wordIndex = (siaLength-1) - elementIndex
    newSia.append(sia[wordIndex])

print(newSia)
